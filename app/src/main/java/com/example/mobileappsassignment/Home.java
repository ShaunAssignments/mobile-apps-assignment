package com.example.mobileappsassignment;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

public class Home extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_favorites:
                        Toast.makeText(Home.this, "Favorites", Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.action_home:
                        Toast.makeText(Home.this, "Home", Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.action_news:
                        Toast.makeText(Home.this, "News", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(Home.this, News.class);
                        startActivity(intent);
                        break;
                }
                return true;
            }
        });
    }
}
