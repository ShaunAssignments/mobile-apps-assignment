package com.example.mobileappsassignment;

public class ArticleClass {
    private String mImageUrl;
    private String mTitle;
    private String mDescription;

    public ArticleClass(String imageUrl, String title, String description){
        mImageUrl = imageUrl;
        mTitle = title;
        mDescription = description;
    }

    public String getImageURL(){
        return mImageUrl;
    }

    public String getTitle(){
        return mTitle;
    }

    public String getDescription(){
        return mDescription;
    }
}
