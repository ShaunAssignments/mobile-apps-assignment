package com.example.mobileappsassignment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder> {

    private Context mContext;
    private ArrayList<ArticleClass> mArticleList;

    public NewsAdapter(Context context, ArrayList<ArticleClass> articleList){
        mContext = context;
        mArticleList = articleList;
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.article_layout, parent, false);
        return new NewsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(NewsViewHolder holder, int position) {
        ArticleClass currentItem = mArticleList.get(position);

        String imageUrl = currentItem.getImageURL();
        String articleTitle = currentItem.getTitle();
        String articleDescription = currentItem.getDescription();

        holder.mTextViewTitle.setText(articleTitle);
        holder.mTextViewDesc.setText(articleDescription);
        Picasso.with(mContext).load(imageUrl).fit().centerInside().into(holder.mImageView);

    }

    @Override
    public int getItemCount() {
        return mArticleList.size();
    }

    public class NewsViewHolder extends RecyclerView.ViewHolder{

        public ImageView mImageView;
        public TextView mTextViewTitle;
        public TextView mTextViewDesc;

        public NewsViewHolder(View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.image_view);
            mTextViewTitle = itemView.findViewById(R.id.text_view_articleTitle);
            mTextViewDesc = itemView.findViewById(R.id.text_view_articleDesc);
        }
    }
}
