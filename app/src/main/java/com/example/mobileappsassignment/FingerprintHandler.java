package com.example.mobileappsassignment;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.M)
public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {

    private Context context;

    public FingerprintHandler(Context context){
        this.context = context;
    }

    public void startAuth(FingerprintManager fingerprintManager,FingerprintManager.CryptoObject cryptoObject ){

        CancellationSignal cancellationSignal = new CancellationSignal();
        fingerprintManager.authenticate(cryptoObject, cancellationSignal,0,this,null);
    }

    @Override
    public void onAuthenticationError(int errorCode, CharSequence errString) {

        this.update("There was an AUTH Error" +errString, false);
    }

    @Override
    public void onAuthenticationFailed() {

        this.update("Auth failed", false);
    }

    @Override
    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {

        this.update("Error: " + helpString, false);
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        this.update("You can now access the app", true);
        context.startActivity(new Intent(context, Home.class));
        Toast.makeText(context,
                "Authentication succeeded.",
                Toast.LENGTH_LONG).show();
    }

    private void update(String s, boolean b) {
        TextView appDeck =  ((Activity)context).findViewById(R.id.appDeck);

        appDeck.setText(s);

        if(b == false) {
            appDeck.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
        }else{
            appDeck.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));
        }
    }
}
