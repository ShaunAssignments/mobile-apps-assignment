package com.example.mobileappsassignment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class News extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private NewsAdapter mNewsAdapter;
    private ArrayList<ArticleClass> mArticleList;
    private RequestQueue mRequestQueue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mArticleList = new ArrayList<>();
        mRequestQueue = Volley.newRequestQueue(this);

        parseJSON();


        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_favorites:
                        Toast.makeText(News.this, "Favorites", Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.action_home:
                        Toast.makeText(News.this, "Home", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(News.this, Home.class);
                        startActivity(intent);
                        break;

                    case R.id.action_news:
                        Toast.makeText(News.this, "News", Toast.LENGTH_SHORT).show();
                        break;
                }
                return false;
            }
        });
    }

    private void parseJSON(){
        String news_url ="https://newsapi.org/v2/top-headlines?country=us&category=technology&apiKey=4cfb11ebd5594e5d88c45b37f715d3c8";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, news_url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("articles");
                    for(int i = 0; i < jsonArray.length(); i++){
                        JSONObject articles = jsonArray.getJSONObject(i);
                        String articleName = articles.getString("title");
                        String articleDesc = articles.getString("description");
                        String imageURL = articles.getString("urlToImage");

                        mArticleList.add(new ArticleClass(imageURL,articleName,articleDesc));

                    }
                    mNewsAdapter = new NewsAdapter(News.this, mArticleList);
                    mRecyclerView.setAdapter(mNewsAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        mRequestQueue.add(request);

    }

    /*public class AsyncHttpTask extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... urls) {
            String result ="";
            URL url;
            HttpURLConnection urlConnection = null;

            try {
                url = new URL (urls[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                String response = streamToString(urlConnection.getInputStream());
                parseResult(response);
                return result;

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    String streamToString(InputStream stream) throws IOException{
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        String data;
        String result = "";

        while ((data = bufferedReader.readLine()) != null){
            result += data;
        }
        if(null != stream){
            stream.close();
        }
        return result;
    }

    private void parseResult(String result){
        JSONObject response = null;
        try {
            response = new JSONObject(result);
            JSONArray articles = response.optJSONArray("articles");

            for(int i = 0; i<10 ; i++){
                JSONObject article = articles.optJSONObject(i);
                String title = article.optString("title");
                Log.i("Titles", title);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }*/
}
